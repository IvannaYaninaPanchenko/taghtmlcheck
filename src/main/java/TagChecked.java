import java.util.Scanner;
import java.util.Stack;

public class TagChecked {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";



    public static void main(String[] args) {
        System.out.println("------------------------------------------------------------");
        System.out.println("Tag Checker will scanner your paragraph to check HTML tags");
        System.out.println("------------------------------------------------------------");
        System.out.println();
        System.out.println("Please, enter you paragraph to be evaluated: ");
        // Using Scanner for Getting Input from User
        Scanner in = new Scanner(System.in);

        String s = in.nextLine();
        System.out.println();

        System.out.println("You entered string "+s);
        System.out.println();
        System.out.println(ANSI_YELLOW+ checkTag(s)+ ANSI_RESET);

    }

    public static String checkTag(String code) {

        var stack = new Stack<String>();

        for(int i = 0; i < code.length();){

            if( i == code.length() && stack.isEmpty()) {return "There is not HTML tags";}

            if(code.startsWith("</", i) && (Character.isUpperCase(code.charAt(i+2)))){

                int j = i + 2;

                i = code.indexOf('>', j);

                if(i < 0 || i == j || i - j > 3) return "Unbalance tag";

                for(int k = j; k < i; k++){
                    if(!Character.isUpperCase(code.charAt(k))) return "The tag should be uppercase";
                }

                String s = code.substring(j, i++);

                String popS = !stack.isEmpty() ? stack.pop(): null;

                if(popS!= null && !popS.equals(s)) return "Expected </"+ popS +"> found </"+ s +">" ;

                if(popS == null) return "Expected # found </"+ s +">";

            // I found the start tag
            } else if(code.startsWith("<", i) && (Character.isUpperCase(code.charAt(i+1)))){

                int j = i + 1;

                i = code.indexOf('>', j);

                if(i < 0 || i == j || i - j > 3) return "Unbalance tag";

                for(int k = j; k < i; k++){
                    if(!Character.isUpperCase(code.charAt(k)))  return "The tag should be uppercase";
                }
                //Push the tag in the Stack
                String s = code.substring(j, i++);
                stack.push(s);
            }else{
                i++;
            }
        }

        if(!stack.isEmpty()){
            String popS = stack.pop();
            return "Expected </"+ popS +"> found #";
        }

        return "Correctly tagged paragraph";
    }
}
