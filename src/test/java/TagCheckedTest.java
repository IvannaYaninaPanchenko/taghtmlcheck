import org.junit.Assert;
import org.junit.Test;

public class TagCheckedTest {

    @Test
    public void checkTag(){
        TagChecked tagChecked = new TagChecked();

        String input = "The following text<C><B>is centred and in boldface</B></C>";
        String input2 = "<B><C> This should be centred and in boldface, but the tags are wrongly nested </B></C>";
        String input3 = "<B>This should be in boldface, but there is an extra closing tag</B></C>";
        String input4 = "<B><C>This should be centred and in boldface, but there is a missing closing tag</C>";

        String result = tagChecked.checkTag(input);
        String result2 = tagChecked.checkTag(input2);
        String result3 = tagChecked.checkTag(input3);
        String result4 = tagChecked.checkTag(input4);

        Assert.assertEquals("Correctly tagged paragraph", result );
        Assert.assertEquals("Expected </C> found </B>", result2 );
        Assert.assertEquals("Expected # found </C>", result3 );
        Assert.assertEquals("Expected </B> found #", result4 );

    }
}
