# Tag HTML Checked

This program validates if a paragraph entered by the user is correct when it has HTML tags.

## Getting Started

1. Check if Java is already installed in your local machine by
   opening CMD in Windows and execute the command  "java - version"
   ![](images/java.PNG)
   
   if you don't have Java go to [Oracle](https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html)

2. Checkout the project in your local machine
   
### Prerequisites

Java need to be installed

### Installing

There are two ways to execute this programs

1. From CMD

    * Open CMD and navigate to the folder where the project has been allocated.
    * Navigate to the folder where is allocated TagChecked.java File
    * Run the command: javac TagChecked.java  
        This command will create a new file TagChecked.class ready to be executed.
    * Run the command: java TagChecked 
       ![](images/compile.PNG)
    
2. From IDE ( For example Intellij) 
    * Install [Intellij](https://www.jetbrains.com/idea/download/#section=windows)
    * Open the project from the disk
    * Open TagChecked class, click right over the class and choose run TagChecked.main()
      Shortcuts: Ctrl + Shift + F10
    

## Running the tests

1. From IDE ( For example Intellij) 
    * Open TagChecked class, click right over the class and choose run TagCheckedTest
      Shortcuts: Ctrl + Shift + F10 

### Break down into end to end tests

evaluates possibles input from the User:
   * The following text<C><B>is centred and in boldface</B></C>
   * <B>This <\g>is <B>boldface</B> in <<*> a</B> <\6> <<d>sentence
   * <B><C> This should be centred and in boldface, but the tags are wrongly nested </B></C>
   * <B>This should be in boldface, but there is an extra closing tag</B></C>
   * <B><C>This should be centred and in boldface, but there is a missing closing tag</C>


## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Ivanna Panchenko** - *Initial work* - [Zurda7](https://IvannaYaninaPanchenko@bitbucket.org/IvannaYaninaPanchenko/taghtmlcheck.git)
